package tabuleiroEntrega;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Scanner;

class Main {

	public static void main(String[] args) {
		Tabuleiro tabuleiro = new TabuleiroFactory().fromDefaultInput();
		new AEstrela(tabuleiro).executar();
	}
}

class AEstrela {

	private PriorityQueue<No> fila;
	private Map<String, No> abertos = new HashMap<String, No>();
	private Map<String, No> fechados = new HashMap<String, No>();

	private final Arvore arvore;
	private No resultado;

	public AEstrela(Tabuleiro tabuleiro) {
		arvore = new Arvore(tabuleiro);
		fila = new Fila().getFila();
	}

	public void executar() {
		
		long ini = System.currentTimeMillis();
		No no = arvore.getRaiz();
		calculaHeuristicas(no);
		adicionaAosAbertos(no);
		faseB();
		
		long fim = System.currentTimeMillis();
		
		System.out.println("\n ===== > "+( fim - ini)+" \n\n");
	}

	private void faseB() {
		while (resultado == null) {

//			if (abertos.isEmpty())
//				throw new RuntimeException("Fracasso");

			No v = menorFDosAbertos();
			fechados.put(v.chave(), v);

			// c
			if (v.isResultadoFinal()) {
				resultado = v;
				System.out.println(v.custoMenorCaminhoAteRaiz());
				return;
			}

			v.geraFilhos();

			if (v.getFilhos().isEmpty())
				continue;

			// d
			for (No m : v.getFilhos()) {
				if (!noExistente(m) || noComGMenorQueExistente(m)) {
					calculaHeuristicas(m);
					adicionaAosAbertos(m);
					removeDosFechados(m);
				}
			}

		}
	}

	private void calculaHeuristicas(No m) {
		// new PrimeiraHeuristica(m).calcular();
		// new SegundaHeuristica(m).calcular();
		new TerceiraHeuristica(m).calcular();
		// new QuartaHeuristica(m).calcular();
		// new QuintaHeuristica(m).calcular();
	}

	private void removeDosFechados(No m) {
		fechados.remove(m.chave());
	}

	private void adicionaAosAbertos(No m) {
		if (abertos.containsKey(m.chave()))
			removeDosAbertos(m);

		abertos.put(m.chave(), m);
		fila.add(m);
	}

	private boolean noComGMenorQueExistente(No m) {
		No existente = abertos.get(m.chave());
		return existente != null
				&& m.custoMenorCaminhoAteRaiz() <= existente
						.custoMenorCaminhoAteRaiz();
	}

	private boolean noExistente(No m) {
		return abertos.containsKey(m.chave())
				|| fechados.containsKey(m.chave());
	}

	private No menorFDosAbertos() {
		No no = fila.remove();
		abertos.remove(no.chave());
		return no;
	}

	private No removeDosAbertos(No no) {
		abertos.remove(no.chave());
		return fila.remove();
	}

}

class Fila {

	public PriorityQueue<No> getFila() {
		return new PriorityQueue<No>(new Comparador());
	}
}

class Comparador implements Comparator<No> {

	@Override
	public int compare(No o1, No o2) {
		return o1.getF() <= o2.getF() ? -1 : 1;
	}
}

class Arvore {

	private No raiz;

	public Arvore(Tabuleiro tabuleiro) {
		this.raiz = new No(tabuleiro, null);
	}

	public No getRaiz() {
		return raiz;
	}

}

class No {

	private String chave;
	private No pai;
	private List<No> filhos = new ArrayList<No>();

	private final Tabuleiro tabuleiro;

	private int hLinha1 = 0;
	private int hLinha3 = 0;
	private int hLinha2 = 0;
	private double hLinha4 = 0;
	private int hLinha5 = 0;

	public No(Tabuleiro tabuleiro, No pai) {
		this.tabuleiro = tabuleiro;
		this.pai = pai;
	}

	public double getF() {
		return custoMenorCaminhoAteRaiz() + hLinha3;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof No) {
			No other = (No) obj;
			return this.tabuleiro.equals(other.tabuleiro);
		}
		return false;
	}

	@Override
	public String toString() {
		return String.valueOf(getF());
	}

	public List<No> getFilhos() {
		return filhos;
	}

	public No getPai() {
		return pai;
	}

	public Tabuleiro getTabuleiro() {
		return tabuleiro;
	}

	public void sethLinha1(int hLinha1) {
		this.hLinha1 = hLinha1;
	}

	public int gethLinha1() {
		return hLinha1;
	}

	public void sethLinha3(int hLinha3) {
		this.hLinha3 = hLinha3;
	}

	public int gethLinha3() {
		return hLinha3;
	}

	public int gethLinha2() {
		return hLinha2;
	}

	public int gethLinha5() {
		return hLinha5;
	}

	public int custoMenorCaminhoAteRaiz() {
		if (pai == null)
			return 0;

		return getPai().custoMenorCaminhoAteRaiz() + 1;
	}

	public void geraFilhos() {
		Movimentador movimentador = new Movimentador();
		geraFilho(movimentador, "baixo");
		geraFilho(movimentador, "cima");
		geraFilho(movimentador, "direita");
		geraFilho(movimentador, "esquerda");
	}

	public String chave() {
		if (chave == null)
			chave = tabuleiro.toString();
		return chave;
	}

	public boolean isResultadoFinal() {
		return this.tabuleiro.equals(new TabuleiroResolvido());
	}

	public void sethLinha4(double d) {
		this.hLinha4 = d;
	}

	public double gethLinha4() {
		return hLinha4;
	}

	private void geraFilho(Movimentador movimento, String mov) {
		Tabuleiro tabulero = tabuleiro.copy();
		tabulero.moveCelulaNulaPara(movimento, mov);
		if (!tabulero.equals(this.tabuleiro) && !tabuleiroIgualDoPai(tabulero))
			filhos.add(new No(tabulero, this));
	}

	private boolean tabuleiroIgualDoPai(Tabuleiro tabuleiro) {
		return getPai() != null && tabuleiro.equals(getPai().getTabuleiro());
	}

	public void sethLinha2(int valor) {
		this.hLinha2 = valor;

	}

	public void sethLinha5(int max) {
		this.hLinha5 = max;
	}

}

abstract class Heuristica {

	protected No no;

	public Heuristica(No no) {
		this.no = no;
	}

	public abstract void calcular();

}

class PrimeiraHeuristica extends Heuristica {

	public PrimeiraHeuristica(No no) {
		super(no);
	}

	@Override
	public void calcular() {
		int numeroDePecasForaDoLugar = 0;
		Celula[][] quadro = no.getTabuleiro().getQuadro();
		for (int i = 0; i < 4; i++)
			for (int j = 0; j < 4; j++)
				if (quadro[i][j].estaForaDoLugar())
					numeroDePecasForaDoLugar++;

		no.sethLinha1(numeroDePecasForaDoLugar);
	}
}

class SegundaHeuristica extends Heuristica {

	public SegundaHeuristica(No no) {
		super(no);
	}

	@Override
	public void calcular() {
		int sequencia = 0;

		Tabuleiro tabuleiro = no.getTabuleiro();
		int anterior = tabuleiro.getCelula(0, 0).getValor();

		for (int i = 1; i < 16; i++) {
			Posicao posicao = posicoes[i];
			int atual = tabuleiro.getCelula(posicao.i, posicao.j).getValor();
			if ((atual - 1) != anterior)
				sequencia++;
		}
		no.sethLinha2(sequencia);
	}

	/**
	 * 1 2 3 4 12 13 14 5 11 0 15 6 10 9 8 7
	 * */

	/*
	 * 00 01 02 03 10 11 12 13 20 21 22 23 30 31 32 33
	 */
	private Posicao[] posicoes = new Posicao[] { new Posicao(0, 0),
			new Posicao(0, 1), new Posicao(0, 2), new Posicao(0, 3),
			new Posicao(1, 3), new Posicao(2, 3), new Posicao(3, 3),
			new Posicao(3, 2), new Posicao(3, 1), new Posicao(3, 0),
			new Posicao(2, 0), new Posicao(1, 0), new Posicao(1, 1),
			new Posicao(1, 2), new Posicao(2, 2), new Posicao(2, 1), };

}

class Posicao {

	public Posicao(int i, int j) {
		this.i = i;
		this.j = j;
	}

	public int i;
	public int j;
}

class TerceiraHeuristica extends Heuristica {

	public TerceiraHeuristica(No no) {
		super(no);
	}

	@Override
	public void calcular() {
		int distancia = 0;
		Tabuleiro tabuleiro = no.getTabuleiro();
		for (int i = 0; i < 4; i++)
			for (int j = 0; j < 4; j++)
				distancia += calculaDistanciaRetangular(tabuleiro.getCelula(i,
						j));

		no.sethLinha3(distancia);
	}

	private int calculaDistanciaRetangular(Celula celula) {
		return distanciaEmAltura(celula) + distanciaEmLargura(celula);
	}

	private int distanciaEmLargura(Celula celula) {
		int distancia = celula.getPosicaoJ() - celula.posicaoCorretaJ();
		if (distancia < 0)
			return celula.posicaoCorretaJ() - celula.getPosicaoJ();

		return distancia;
	}

	private int distanciaEmAltura(Celula celula) {
		int distancia = celula.getPosicaoI() - celula.posicaoCorretaI();
		if (distancia < 0)
			return celula.posicaoCorretaI() - celula.getPosicaoI();

		return distancia;
	}

}

class QuartaHeuristica extends Heuristica {

	private double p1 = 0;
	private double p2 = 0;
	private double p3 = 1;

	public QuartaHeuristica(No no) {
		super(no);
	}

	@Override
	public void calcular() {
		no.sethLinha4(no.gethLinha1() * p1 + no.gethLinha3() * p3
				+ no.gethLinha2() * p2);
	}

}

class QuintaHeuristica extends Heuristica {

	public QuintaHeuristica(No no) {
		super(no);
	}

	@Override
	public void calcular() {
		int primeiro = Integer.max(no.gethLinha1(), no.gethLinha2());
		no.sethLinha5(Integer.max(primeiro, no.gethLinha3()));
	}

}

class Tabuleiro {

	private String toString;

	protected Celula quadro[][];

	public Tabuleiro(Celula quadro[][]) {
		this.quadro = quadro;
	}

	protected Tabuleiro() {
	}

	public Celula[][] getQuadro() {
		return quadro;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Tabuleiro) {
			Tabuleiro other = (Tabuleiro) obj;

			return this.toString().equals(other.toString());
		}
		return false;
	}

	@Override
	public String toString() {
		if (toString == null) {
			StringBuilder builder = new StringBuilder();
			for (int i = 0; i < 4; i++) {
				for (int j = 0; j < 4; j++) {
					builder.append(quadro[i][j]).append(" ");
				}
				builder.append(System.lineSeparator());
			}
			toString = builder.toString();
		}
		return toString;
	}

	public Celula getCelula(int i, int j) {
		return quadro[i][j];
	}

	public Tabuleiro copy() {
		return new TabuleiroFactory().copy(this);
	}

	public CelulaNula getCelulaNula() {
		for (int i = 0; i < 4; i++)
			for (int j = 0; j < 4; j++)
				if (quadro[i][j].isCelulaNula())
					return (CelulaNula) quadro[i][j];

		return null;
	}

	public void atualizaPeca(Celula celula) {
		this.quadro[celula.getPosicaoI()][celula.getPosicaoJ()] = celula;
	}

	public void moveCelulaNulaPara(Movimentador movimento, String mov) {
		movimento.mover(mov, this);
	}
}

class TabuleiroResolvido extends Tabuleiro {

	public TabuleiroResolvido() {
		Celula c1 = new CelulaFactory().newInstance(1, 0, 0);
		Celula c2 = new CelulaFactory().newInstance(2, 0, 1);
		Celula c3 = new CelulaFactory().newInstance(3, 0, 2);
		Celula c4 = new CelulaFactory().newInstance(4, 0, 3);

		Celula c5 = new CelulaFactory().newInstance(12, 1, 0);
		Celula c6 = new CelulaFactory().newInstance(13, 1, 1);
		Celula c7 = new CelulaFactory().newInstance(14, 1, 2);
		Celula c8 = new CelulaFactory().newInstance(5, 1, 3);

		Celula c9 = new CelulaFactory().newInstance(11, 2, 0);
		Celula c10 = new CelulaFactory().newInstance(16, 2, 1);
		Celula c11 = new CelulaFactory().newInstance(15, 2, 2);
		Celula c12 = new CelulaFactory().newInstance(6, 2, 3);

		Celula c13 = new CelulaFactory().newInstance(10, 3, 0);
		Celula c14 = new CelulaFactory().newInstance(9, 3, 1);
		Celula c15 = new CelulaFactory().newInstance(8, 3, 2);
		Celula c16 = new CelulaFactory().newInstance(7, 3, 3);

		this.quadro = new Celula[][] { { c1, c2, c3, c4 }, { c5, c6, c7, c8 },
				{ c9, c10, c11, c12 }, { c13, c14, c15, c16 } };
	}

}

class TabuleiroFactory {

	public Tabuleiro fromFile(String path) throws FileNotFoundException {
		FileInputStream file = new FileInputStream(path);
		Scanner scanner = new Scanner(file);

		return montaTabuleiro(scanner);
	}

	public Tabuleiro fromDefaultInput() {
		Scanner scanner = new Scanner(System.in);
		return montaTabuleiro(scanner);
	}

	public Tabuleiro copy(Tabuleiro tabuleiro) {
//		Scanner scanner = new Scanner(tabuleiro.toString());
//		return montaTabuleiro(scanner);
		Celula[][] quadroNovo = new Celula[4][4];
		for (int i = 0; i < 4; i++)
			for (int j = 0; j < 4; j++) {
				Celula celula = tabuleiro.getCelula(i, j).copy();
				quadroNovo[i][j] = celula;
			}
		
		return new Tabuleiro(quadroNovo);
	}

	private Tabuleiro montaTabuleiro(Scanner scanner) {
		Celula[][] quadro = new Celula[4][4];
		for (int i = 0; i < 4; i++)
			for (int j = 0; j < 4; j++)
				quadro[i][j] = new CelulaFactory().newInstance(
						scanner.nextInt(), i, j);

		scanner.close();
		return new Tabuleiro(quadro);
	}
}

class CelulaFactory {

	public Celula newInstance(int valor, int posicaoI, int posicaoJ) {
		switch (valor) {
		case 1:
			return new Celula1(posicaoI, posicaoJ);
		case 2:
			return new Celula2(posicaoI, posicaoJ);
		case 3:
			return new Celula3(posicaoI, posicaoJ);
		case 4:
			return new Celula4(posicaoI, posicaoJ);
		case 5:
			return new Celula5(posicaoI, posicaoJ);
		case 6:
			return new Celula6(posicaoI, posicaoJ);
		case 7:
			return new Celula7(posicaoI, posicaoJ);
		case 8:
			return new Celula8(posicaoI, posicaoJ);
		case 9:
			return new Celula9(posicaoI, posicaoJ);
		case 10:
			return new Celula10(posicaoI, posicaoJ);
		case 11:
			return new Celula11(posicaoI, posicaoJ);
		case 12:
			return new Celula12(posicaoI, posicaoJ);
		case 13:
			return new Celula13(posicaoI, posicaoJ);
		case 14:
			return new Celula14(posicaoI, posicaoJ);
		case 15:
			return new Celula15(posicaoI, posicaoJ);
		default:
			return new CelulaNula(posicaoI, posicaoJ);
		}
	}

}

abstract class Celula {

	protected int posicaoI;
	protected int posicaoJ;

	public Celula(int posicaoI, int posicaoJ) {
		this.posicaoI = posicaoI;
		this.posicaoJ = posicaoJ;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Celula) {
			Celula other = (Celula) obj;
			return this.getValor() == other.getValor();
		}
		return false;
	}

	@Override
	public String toString() {
		return String.valueOf(getValor());
	}

	public boolean estaForaDoLugar() {
		return posicaoI != posicaoCorretaI() || posicaoJ != posicaoCorretaJ();
	}

	public int getPosicaoI() {
		return posicaoI;
	}

	public int getPosicaoJ() {
		return posicaoJ;
	}

	public void vaiParaLugarQueEstavaNulo(CelulaNula nula) {
		this.posicaoI = nula.getPosicaoI();
		this.posicaoJ = nula.getPosicaoJ();
	}

	public Celula copy() {
		return new CelulaFactory().newInstance(getValor(), posicaoI, posicaoJ);
	}

	public boolean isCelulaNula() {
		return false;
	}

	public abstract int getValor();

	public abstract int posicaoCorretaI();

	public abstract int posicaoCorretaJ();

}

class CelulaNula extends Celula {

	public CelulaNula(int posicaoI, int posicaoJ) {
		super(posicaoI, posicaoJ);
	}

	@Override
	public int posicaoCorretaI() {
		return 2;
	}

	@Override
	public int posicaoCorretaJ() {
		return 1;
	}

	@Override
	public int getValor() {
		return 0;
	}

	public void moveParaCima(Tabuleiro tabuleiro) {
		if (posicaoI > 0)
			atualizaCelulas(tabuleiro, posicaoI - 1, posicaoJ);
	}

	public void moveParaBaixo(Tabuleiro tabuleiro) {
		if (posicaoI < 3)
			atualizaCelulas(tabuleiro, posicaoI + 1, posicaoJ);
	}

	public void moveParaDireita(Tabuleiro tabuleiro) {
		if (posicaoJ < 3)
			atualizaCelulas(tabuleiro, posicaoI, posicaoJ + 1);
	}

	public void moveParaEsquerda(Tabuleiro tabuleiro) {
		if (posicaoJ > 0)
			atualizaCelulas(tabuleiro, posicaoI, posicaoJ - 1);

	}

	public void atualizaCelulas(Tabuleiro tabuleiro, int i, int j) {
		Celula celula = tabuleiro.getCelula(i, j);
		trocaCom(celula);
		tabuleiro.atualizaPeca(this);
		tabuleiro.atualizaPeca(celula);
	}

	private void trocaCom(Celula outra) {
		if (outra != this) {
			int i = outra.getPosicaoI();
			int j = outra.getPosicaoJ();
			outra.vaiParaLugarQueEstavaNulo(this);
			this.posicaoI = i;
			this.posicaoJ = j;
		}
	}

	@Override
	public boolean isCelulaNula() {
		return true;
	}

}

class Celula1 extends Celula {

	public Celula1(int posicaoI, int posicaoJ) {
		super(posicaoI, posicaoJ);
	}

	@Override
	public int posicaoCorretaI() {
		return 0;
	}

	@Override
	public int posicaoCorretaJ() {
		return 0;
	}

	@Override
	public int getValor() {
		return 1;
	}
}

class Celula2 extends Celula {

	public Celula2(int posicaoI, int posicaoJ) {
		super(posicaoI, posicaoJ);
	}

	@Override
	public int posicaoCorretaI() {
		return 0;
	}

	@Override
	public int posicaoCorretaJ() {
		return 1;
	}

	@Override
	public int getValor() {
		return 2;
	}

}

class Celula3 extends Celula {

	public Celula3(int posicaoI, int posicaoJ) {
		super(posicaoI, posicaoJ);
	}

	@Override
	public int posicaoCorretaI() {
		return 0;
	}

	@Override
	public int posicaoCorretaJ() {
		return 2;
	}

	@Override
	public int getValor() {
		return 3;
	}

}

class Celula4 extends Celula {

	public Celula4(int posicaoI, int posicaoJ) {
		super(posicaoI, posicaoJ);
	}

	@Override
	public int posicaoCorretaI() {
		return 0;
	}

	@Override
	public int posicaoCorretaJ() {
		return 3;
	}

	@Override
	public int getValor() {
		return 4;
	}

}

class Celula5 extends Celula {

	public Celula5(int posicaoI, int posicaoJ) {
		super(posicaoI, posicaoJ);
	}

	@Override
	public int posicaoCorretaI() {
		return 1;
	}

	@Override
	public int posicaoCorretaJ() {
		return 3;
	}

	@Override
	public int getValor() {
		return 5;
	}

}

class Celula6 extends Celula {

	public Celula6(int posicaoI, int posicaoJ) {
		super(posicaoI, posicaoJ);
	}

	@Override
	public int posicaoCorretaI() {
		return 2;
	}

	@Override
	public int posicaoCorretaJ() {
		return 3;
	}

	@Override
	public int getValor() {
		return 6;
	}
}

class Celula7 extends Celula {

	public Celula7(int posicaoI, int posicaoJ) {
		super(posicaoI, posicaoJ);
	}

	@Override
	public int posicaoCorretaI() {
		return 3;
	}

	@Override
	public int posicaoCorretaJ() {
		return 3;
	}

	@Override
	public int getValor() {
		return 7;
	}
}

class Celula8 extends Celula {

	public Celula8(int posicaoI, int posicaoJ) {
		super(posicaoI, posicaoJ);
	}

	@Override
	public int posicaoCorretaI() {
		return 3;
	}

	@Override
	public int posicaoCorretaJ() {
		return 2;
	}

	@Override
	public int getValor() {
		return 8;
	}
}

class Celula9 extends Celula {

	public Celula9(int posicaoI, int posicaoJ) {
		super(posicaoI, posicaoJ);
	}

	@Override
	public int posicaoCorretaI() {
		return 3;
	}

	@Override
	public int posicaoCorretaJ() {
		return 1;
	}

	@Override
	public int getValor() {
		return 9;
	}

}

class Celula10 extends Celula {

	public Celula10(int posicaoI, int posicaoJ) {
		super(posicaoI, posicaoJ);
	}

	@Override
	public int posicaoCorretaI() {
		return 3;
	}

	@Override
	public int posicaoCorretaJ() {
		return 0;
	}

	@Override
	public int getValor() {
		return 10;
	}

}

class Celula11 extends Celula {

	public Celula11(int posicaoI, int posicaoJ) {
		super(posicaoI, posicaoJ);
	}

	@Override
	public int posicaoCorretaI() {
		return 2;
	}

	@Override
	public int posicaoCorretaJ() {
		return 0;
	}

	@Override
	public int getValor() {
		return 11;
	}

}

class Celula12 extends Celula {

	public Celula12(int posicaoI, int posicaoJ) {
		super(posicaoI, posicaoJ);
	}

	@Override
	public int posicaoCorretaI() {
		return 1;
	}

	@Override
	public int posicaoCorretaJ() {
		return 0;
	}

	@Override
	public int getValor() {
		return 12;
	}

}

class Celula13 extends Celula {

	public Celula13(int posicaoI, int posicaoJ) {
		super(posicaoI, posicaoJ);
	}

	@Override
	public int posicaoCorretaI() {
		return 1;
	}

	@Override
	public int posicaoCorretaJ() {
		return 1;
	}

	@Override
	public int getValor() {
		return 13;
	}

}

class Celula14 extends Celula {

	public Celula14(int posicaoI, int posicaoJ) {
		super(posicaoI, posicaoJ);
	}

	@Override
	public int posicaoCorretaI() {
		return 0;
	}

	@Override
	public int posicaoCorretaJ() {
		return 2;
	}

	@Override
	public int getValor() {
		return 14;
	}

}

class Celula15 extends Celula {

	public Celula15(int posicaoI, int posicaoJ) {
		super(posicaoI, posicaoJ);
	}

	@Override
	public int posicaoCorretaI() {
		return 2;
	}

	@Override
	public int posicaoCorretaJ() {
		return 2;
	}

	@Override
	public int getValor() {
		return 15;
	}

}

class Movimentador {

	public void mover(String movimento, Tabuleiro tabuleiro) {
		if ("cima".equals(movimento))
			cima(tabuleiro);

		if ("baixo".equals(movimento))
			baixo(tabuleiro);

		if ("direita".equals(movimento))
			direita(tabuleiro);

		if ("esquerda".equals(movimento))
			esquerda(tabuleiro);
	}

	public void cima(Tabuleiro tabuleiro) {
		tabuleiro.getCelulaNula().moveParaCima(tabuleiro);
	}

	public void baixo(Tabuleiro tabuleiro) {
		tabuleiro.getCelulaNula().moveParaBaixo(tabuleiro);
	}

	public void direita(Tabuleiro tabuleiro) {
		tabuleiro.getCelulaNula().moveParaDireita(tabuleiro);
	}

	public void esquerda(Tabuleiro tabuleiro) {
		tabuleiro.getCelulaNula().moveParaEsquerda(tabuleiro);
	}

}
